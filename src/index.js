const express = require("express");
const { createProxyMiddleware } = require("http-proxy-middleware");
require("dotenv").config();

const routes = require("./proxy");

const app = express();

app.use(express.static(process.env.DATA_STUDIO_PATH));
app.get("/", function (req, res) {
  res.sendFile(`${process.env.DATA_STUDIO_PATH}/index.html`);
});

routes.forEach((route) => {
  app.use(route.key, createProxyMiddleware(route.proxy));
});

app.listen(3000);
