const fs = require("fs");
const path = require("path");
const { createProxyMiddleware } = require("http-proxy-middleware");

function getSessionId() {
  let sessionId;
  try {
    const cookies = fs.readFileSync(
      path.join(__dirname, "../sessions/cookies.txt"),
      {
        encoding: "utf8",
        flag: "r",
      }
    );
    const searchFor = new RegExp(/\/client.*JSESSIONID.*/, "im");
    const appSession = cookies.match(searchFor);
    const result = appSession?.[0]
      ? appSession[0].match(/([a-z0-9.]+)$/gi)
      : null;

    sessionId = `JSESSIONID=${result?.[0]}`;
  } catch {}

  return sessionId;
}

const sessionId = getSessionId();

const routes = [];

routes.push({
  key: "/ngta",
  proxy: {
    target: `${process.env.PROXY_TARGET}/ngta`,
    changeOrigin: true,
    secure: false,
    router: function (req) {
      return `${process.env.PROXY_TARGET}`;
    },
    headers: {
      Cookie: sessionId,
      Connection: "keep-alive",
      Referer: `${process.env.PROXY_TARGET}/index.html`,
    },
    logLevel: "debug",
  },
});

module.exports = routes;
