# Datastudio proxy

- Change the settings in the `.env` file
- Save the current session cookies file `cookies.txt` to `./sessions/cookies.txt`
- Install dependencies with `yarn install`
- Run with `yarn start`
- Open the app at `http://localhost:3000`
- Develop as usual with `yarn dev` in the source branch
